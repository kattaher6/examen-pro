package com.examen;

import java.util.Scanner;

public class Palindromo implements Interface{
	
	@Override
	
	public void identificarPalindromo() {
		
		Scanner lector=new Scanner(System.in);
		String palabra,reves="";
		System.out.print("Escriba una palabra: ");
		palabra=lector.nextLine();
		for(int i=palabra.length()-1; i >= 0; i--) {
			reves+=palabra.charAt(i);
		}
		
		if(palabra.equals(reves)) {
			System.out.print("\n SI es palindromo");
		}else {
			System.out.print("\n NO es palindromo");
		}
		
	}

}
